#ifndef WORKER_H
#define WORKER_H

#include <unordered_map>
#include <map>
#include <string>
#include <thread>
#include <memory>

#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/BaseNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Link.h>

struct threadMap
{
    bool _isRunning;
    std::unique_ptr<std::thread> _thread;
};

class Worker
{
public:
    Worker();
    void run();
    void stop();
    void addLink(BaseNode* out, BaseNode* in);
    void addNode(BaseNode* node);
    void removeNode(BaseNode* node);
    ~Worker();

private:
    void MStopNode(BaseNode* node);
    void MProcessNode(BaseNode* node);
    Link* MFindLink(const std::string& name);

    std::map<BaseNode*, std::unique_ptr<threadMap>> m_nodeMapPtr;
    std::map<std::string, Link*> m_links;

};

#endif // WORKER_H
