#ifndef THRESHOLDNODE_H
#define THRESHOLDNODE_H

#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/BaseNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassMat.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/OutputClasses/OutputClassMat.h>


class ThresholdNode : public BaseNode
{
public:

    ThresholdNode(const std::string& name);

protected:

    void compute() override;
    void prepare() override;


private:
    cv::Mat m_inImage;
    cv::Mat m_outImage;

    int m_thresh;
};

#endif // THRESHOLDNODE_H
