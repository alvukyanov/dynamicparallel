#ifndef BASENODE_H
#define BASENODE_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClass.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Types.h>
#include <vector>
#include <variant>
#include <opencv2/core.hpp>

using types = std::variant<cv::Mat, int, float, dot> ;


class BaseNode
{
public:
    BaseNode();
    BaseNode(const std::string& name);

    void init();

    IOBaseClass* getInput(const std::string& name);
    std::vector<IOBaseClass*> getAllInputs();


    IOBaseClass* getOutput(const std::string& name);
    std::vector<IOBaseClass*> getAllOutputs();

    // get Node Stats
    void setName(const std::string& name);
    std::string getName();

    void run();

    void addInput(IOBaseClass* input);

    void addOutput(IOBaseClass* output);

    virtual void prepare();

    virtual ~BaseNode();

protected:

    virtual void compute();

    std::string m_name;

    std::vector<IOBaseClass*> m_inputs;

    std::vector<IOBaseClass*> m_outputs;

    std::vector<types> m_dataVector;

    IOBaseClass* MGetItem(std::vector<IOBaseClass*> dataVector, const std::string& name);

    std::vector<IOBaseClass*> MGetItems(std::vector<IOBaseClass*> dataVector);

private:



};



#endif // NODE_H



