#ifndef OUTPUTNODE_H
#define OUTPUTNODE_H
#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/BaseNode.h>
#include <opencv2/core.hpp>


class OutputNode : public BaseNode
{
public:

    OutputNode();

    OutputNode(const std::string& name, const std::string& path);

    void setImagePath(const std::string &imagePath);

    void prepare() override;

protected:

    void compute() override;

private:

    cv::Mat m_img;
    std::string m_imagePath;
};

#endif // INPUTNODE_H
