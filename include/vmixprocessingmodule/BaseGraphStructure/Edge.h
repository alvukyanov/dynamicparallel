#ifndef EDGE_H
#define EDGE_H
#include <include/vmixprocessingmodule/BaseGraphStructure/Vertex.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Link.h>

#include <vector>
#include <initializer_list>

class Edge
{
public:
    Edge();
    Edge(std::shared_ptr<Vertex> leftVertex, std::shared_ptr<Vertex> rightVertex, std::initializer_list<std::shared_ptr<Link>>& links);
    Edge(std::shared_ptr<Vertex> leftVertex, std::shared_ptr<Vertex> rightVertex);


    void addLink(std::shared_ptr<Link>& link);
    void removeLink(std::shared_ptr<Link>& link);

    void setLeftVertex(std::shared_ptr<Vertex> leftVertex);
    void setRightVertex(std::shared_ptr<Vertex> rightVertex);


    std::shared_ptr<Vertex> getLeftVertex();
    std::shared_ptr<Vertex> getRightVertex();

    void setLength(const int& len);
    int getLetngth();


private:

    std::shared_ptr<Vertex> m_leftVertex;
    std::shared_ptr<Vertex> m_rightVertex;


    std::vector<std::shared_ptr<Link>> m_links;
    int m_length;
};

#endif // EDGE_H
