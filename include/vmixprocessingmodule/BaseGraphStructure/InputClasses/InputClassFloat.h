//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_INPUTCLASSINT_H
#define VMIXPROCESSINGMODULE_INPUTCLASSINT_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClass.h>

class InputClassFloat: public IOBaseClass
{
public:

    int getType() override;
    void setData(float data);
    float getData();

protected:
    float m_data;
};

#endif //VMIXPROCESSINGMODULE_INPUTCLASSINT_H
