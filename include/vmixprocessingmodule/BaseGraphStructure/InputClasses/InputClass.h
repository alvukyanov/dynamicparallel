#ifndef INPUTCLASS_H
#define INPUTCLASS_H

#include <string>

class BaseNode;

class IOBaseClass
{
public:

    IOBaseClass(const std::string& name);

    IOBaseClass(const std::string& name, BaseNode* node);

    virtual int getType();

    [[nodiscard]] int getTact() const;

    std::string getName();

    virtual ~IOBaseClass();

protected:

    int m_tact;
    BaseNode* m_node{};
    std::string m_name;
};

#endif // INPUTCLASS_H




