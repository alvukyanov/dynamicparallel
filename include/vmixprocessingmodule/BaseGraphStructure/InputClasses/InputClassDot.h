
//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_INPUTCLASSINT_H
#define VMIXPROCESSINGMODULE_INPUTCLASSINT_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClass.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Types.h>


class InputClassDot : public IOBaseClass
{
public:
    int getType() override;

    void setData(dot data);
    dot getData();

private:
    dot m_data;
};

#endif //VMIXPROCESSINGMODULE_INPUTCLASSINT_H
