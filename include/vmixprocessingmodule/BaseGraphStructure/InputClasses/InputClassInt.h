//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_INPUTCLASSINT_H
#define VMIXPROCESSINGMODULE_INPUTCLASSINT_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClass.h>

class InputClassInt : public IOBaseClass
{
public:

    int getType() override;
    void setData(int data);
    int getData();


protected:
    int m_data;
};

#endif //VMIXPROCESSINGMODULE_INPUTCLASSINT_H
