//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_INPUTCLASSINT_H
#define VMIXPROCESSINGMODULE_INPUTCLASSINT_H

#include <opencv2/core.hpp>
#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClass.h>

class InputClassMat : public IOBaseClass
{
public:

    InputClassMat(const std::string& name);

    InputClassMat(const std::string& name, BaseNode* node);

    int getType() override;

    void setData(cv::Mat data);
    cv::Mat getData();

protected:
    cv::Mat m_data;
};

#endif //VMIXPROCESSINGMODULE_INPUTCLASSINT_H
