#ifndef VERTEX_H
#define VERTEX_H

#include <string>
#include <memory>

#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/BaseNode.h>

class Vertex : std::enable_shared_from_this<Vertex>
{
public:
    Vertex(const std::string &name);
    Vertex(BaseNode* node, const std::string& name);


    void setVertexName(const std::string &name);
    std::string getVertexName();

    void setTact(const int& tact);
    int getTact();

    void setNode(BaseNode* node);
    BaseNode* getNode();
    ~Vertex();

private:
    std::string m_name;
    int m_tactId;

    BaseNode* m_node;

};

#endif // VERTEX_H
