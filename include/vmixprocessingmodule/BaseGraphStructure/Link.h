#ifndef LINK_H
#define LINK_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClass.h>
#include <vector>


class Link
{
public:

    Link(const std::string& input = "input", const std::string& output = "output");
    void createLink(IOBaseClass* input, IOBaseClass* output);
    IOBaseClass* getInput();
    IOBaseClass* getOutput();
    ~Link();

private:

    IOBaseClass* m_input;
    IOBaseClass* m_output;
};

#endif // LINK_H
