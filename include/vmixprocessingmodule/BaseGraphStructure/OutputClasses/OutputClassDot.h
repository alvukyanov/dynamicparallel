
//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_OUTPUTCLASSDOT_H
#define VMIXPROCESSINGMODULE_OUTPUTCLASSDOT_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassDot.h>


class OutputClassDot : public InputClassDot
{
public:
    void copy(InputClassDot* input);
};

#endif //VMIXPROCESSINGMODULE_OUTPUTCLASSDOT_H
