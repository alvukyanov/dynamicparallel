//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_OUTPUTCLASSMAT_H
#define VMIXPROCESSINGMODULE_OUTPUTCLASSMAT_H

#include <opencv2/core.hpp>
#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassMat.h>

class OutputClassMat : public InputClassMat
{
public:

    OutputClassMat(const std::string& name);
    OutputClassMat(const std::string& name, BaseNode* node);
    InputClassMat* copy(InputClassMat* input);

};

#endif //VMIXPROCESSINGMODULE_INPUTCLASSINT_H
