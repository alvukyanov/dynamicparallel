//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_OUTPUTCLASSFLOAT_H
#define VMIXPROCESSINGMODULE_OUTPUTCLASSFLOAT_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassFloat.h>

class OutputClassFloat: public InputClassFloat
{
public:
    void copy(InputClassFloat* input);
};

#endif //VMIXPROCESSINGMODULE_OUTPUTCLASSFLOAT_H
