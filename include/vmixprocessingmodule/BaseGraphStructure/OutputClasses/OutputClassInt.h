//
// Created by arlukyanov on 15.12.23.
//

#ifndef VMIXPROCESSINGMODULE_OUTPUTCLASSINT_H
#define VMIXPROCESSINGMODULE_OUTPUTCLASSINT_H

#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassInt.h>

class OutputClassInt : public InputClassInt
{
public:
    OutputClassInt(const std::string& name);
    void copy(InputClassInt* input);
};

#endif //VMIXPROCESSINGMODULE_INPUTCLASSINT_H
