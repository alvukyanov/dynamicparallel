#include <chrono>
#include <iostream>
#include <include/vmixprocessingmodule/PipelineProcessors/Worker.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/ThresholdNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/InputNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/OutputNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/BaseNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Link.h>


int main(int argc, char *argv[])
{
    std::shared_ptr<Worker> wkr = std::make_shared<Worker>();

    BaseNode* n1 = new InputNode("InputNode", "/home/arlukyanov/vmix/vmixprocessingmodule/apple.jpg");
    BaseNode* n2 = new ThresholdNode("threshold");
    BaseNode* n3 = new OutputNode("OutputAfterThreshold", "/home/arlukyanov/vmix/vmixprocessingmodule/appleThresh.jpg");
    wkr->addNode(n1);
    wkr->addNode(n2);
    wkr->addNode(n3);
    wkr->addLink(n1, n2);
    wkr->addLink(n2, n3);

    wkr->run();
    return 0;
}
