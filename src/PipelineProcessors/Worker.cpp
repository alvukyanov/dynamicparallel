#include <include/vmixprocessingmodule/BaseGraphStructure/OutputClasses/OutputClassMat.h>
#include <include/vmixprocessingmodule/PipelineProcessors/Worker.h>
#include <iostream>
\

Worker::Worker()
{

}

void Worker::addNode(BaseNode* node)
{

    // добавляем ноду
    m_nodeMapPtr.insert(std::make_pair(node, std::move(std::make_unique<threadMap>())));

    /// TODO: Добавляем вертекс

    std::cout << "Worker::addNode : " << node->getName() << " finished\n";
}

void Worker::addLink(BaseNode* out, BaseNode* in)
{
    auto outputName = out->getAllOutputs()[0]->getName();
    auto inputName = out->getAllInputs()[0]->getName();
    Link* lnk = new Link(outputName, inputName);
    auto name = outputName + inputName + "Link";
    lnk->createLink(out->getAllOutputs()[0], in->getAllInputs()[0]);
    m_links.insert(std::make_pair(name, lnk));
}


void Worker::run()
{
    std::cout << "Worker::run\n";
    for(auto &node : m_nodeMapPtr)
    {
        node.second->_isRunning = true;
        node.first->prepare();
        node.second->_thread = std::make_unique<std::thread>(&Worker::MProcessNode, this, std::ref(node.first));
        std::cout << "Starting node: " << node.first->getName() << "\n";
    }
    std::cout << "Worker::run finished\n";
}

void Worker::removeNode(BaseNode *node)
{
    /*
    std::cout << "Worker::removeNode " << node << "\n";
    for(auto it = m_nodeMapPtr.begin(); it != m_nodeMapPtr.end();)
    {
        if(it->first == node)
        {
            if(it->second->_isRunning)
                MStopNode(it->first);

            auto name = it->first;
            it = m_nodeMapPtr.erase(it);
            std::cout << "Removed node: " << name << "\n";
        }
        else
        {
            it++;
        }
    }
    std::cout << "Worker::removeNode finished\n";
    */
}

void Worker::MStopNode(BaseNode* node)
{
    /*
    m_nodeMapPtr.at(node)->_isRunning = false;
    if(m_nodeMapPtr.at(node)->_thread->joinable())
        m_nodeMapPtr.at(node)->_thread->join();
    std::cout << "Node " << node << " stopped; \n";
    */
}

void Worker::stop()
{

    std::cout << "Worker::stop()\n";
    for(auto &node : m_nodeMapPtr)
    {
        node.second->_isRunning = false;
        if(node.second->_thread->joinable())
            node.second->_thread->join();
        std::cout << "Node " << node.first->getName() << " exited; \n";
    }
    std::cout << "Worker::stop() finished\n";
}

void Worker::MProcessNode(BaseNode* node)
{
    node->run();
    for(auto &link : m_links)
    {
        if(link.first.find(node->getAllOutputs()[0]->getName()) == true)
        {
            dynamic_cast<InputClassMat*>(link.second->getInput())->setData(
                dynamic_cast<OutputClassMat*>(node->getAllOutputs()[0])->copy(
                                                                            dynamic_cast<InputClassMat*>(link.second->getInput()))->getData());
        }
    }
}

Worker::~Worker()
{
    stop();
}











