#include "include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassInt.h"
#include <include/vmixprocessingmodule/BaseGraphStructure/Types.h>

int InputClassInt::getType()
{
    return Types::INT;
}

int InputClassInt::getData()
{
    return m_data;
}


void InputClassInt::setData(int data)
{
    m_data = data;
}
