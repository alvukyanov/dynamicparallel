#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassDot.h>



int InputClassDot::getType()
{
    return Types::DOT;
}


dot InputClassDot::getData()
{
    return m_data;
}

void InputClassDot::setData(dot data)
{
    m_data = data;
}
