#include <typeinfo>
#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClass.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/BaseNode.h>


IOBaseClass::IOBaseClass(const std::string& name)
{
    m_tact = 0;
    m_node = new BaseNode();
    m_name = name;
}

IOBaseClass::IOBaseClass(const std::string& name, BaseNode* node)
{
    m_tact = 0;
    m_node = node;
    m_name = name;
}

int IOBaseClass::getTact() const
{
    return m_tact;
}

std::string IOBaseClass::getName()
{
    return m_name;
}

IOBaseClass::~IOBaseClass()
{
    delete m_node;
}

int IOBaseClass::getType()
{
    return 5;
}
