#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassMat.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Types.h>

InputClassMat::InputClassMat(const std::string &name) : IOBaseClass(name) {};

InputClassMat::InputClassMat(const std::string &name, BaseNode* node) : IOBaseClass(name, node) {};

int InputClassMat::getType()
{
    return Types::MATRIX;
}

cv::Mat InputClassMat::getData()
{
    return m_data;
}

void InputClassMat::setData(cv::Mat data)
{
    m_data = data;
}
