#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassFloat.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/Types.h>

int InputClassFloat::getType()
{
    return Types::FLOAT;
}

float InputClassFloat::getData()
{
    return m_data;
}

void InputClassFloat::setData(float data)
{
    m_data = data;
}
