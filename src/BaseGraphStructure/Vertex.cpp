#include <include/vmixprocessingmodule/BaseGraphStructure/Vertex.h>

Vertex::Vertex(const std::string& name)
{
    m_tactId = 0;
    m_name = name;
    m_node = new BaseNode();
}

Vertex::Vertex(BaseNode* node, const std::string& name)
{
    m_node = node;
    m_name = name;
    m_tactId = 0;
}


void Vertex::setVertexName(const std::string &name)
{
    m_name = name;
}

std::string Vertex::getVertexName()
{
    return m_name;
}

void Vertex::setTact(const int& tact)
{
    m_tactId = tact;
}

int Vertex::getTact()
{
    return m_tactId;
}

void Vertex::setNode(BaseNode* node)
{
    m_node = node;
}

BaseNode* Vertex::getNode()
{
    return m_node;
}

Vertex::~Vertex()
{
    delete m_node;
}
