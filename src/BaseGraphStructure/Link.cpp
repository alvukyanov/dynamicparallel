#include <include/vmixprocessingmodule/BaseGraphStructure/Link.h>
#include <iostream>
#include <stdexcept>

Link::Link(const std::string& input, const std::string& output)
{
    m_input = new IOBaseClass(input);
    m_output = new IOBaseClass(output);
}


void Link::createLink(IOBaseClass *input, IOBaseClass *output)
{
    try {
        if(input->getType() == output->getType())
        {
            m_input = input;
            m_output = output;
        }
        else
        {
            throw std::invalid_argument("Input::getType() and Output::getType() doesnt't match. Only one type required.");
        }

    } catch (std::invalid_argument &e) {
        std::cerr << e.what();
    }

}

IOBaseClass* Link::getInput()
{
    return m_input;
}

IOBaseClass* Link::getOutput()
{
    return m_output;
}


Link::~Link()
{
    delete m_input;
    delete m_output;
}
