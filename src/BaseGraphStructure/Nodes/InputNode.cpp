#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/InputNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassMat.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/OutputClasses/OutputClassMat.h>
#include <opencv2/imgcodecs.hpp>
#include <iostream>


InputNode::InputNode(const std::string& name, const std::string& path) : BaseNode(name)
{
    m_imagePath = path;
    addInput(new InputClassMat(name + "Input", this));
    addOutput(new OutputClassMat(name + "Output", this));
}


void InputNode::prepare()
{
    // or read data from disk
    try {
        m_img = cv::imread(m_imagePath, cv::IMREAD_COLOR);
        if (m_img.empty()) {
            throw cv::Exception(404,
                                "No such file or directory",
                                "void InputNode::prepare()",
                                "InputNode.cpp",
                                17
            );
        }
    } catch(cv::Exception& e) {
        std::cerr << "Exception at :" << e.file
                << "\nType : " << e.code << " : " << e.err
                << "\nIn functuon : " << e.func
                << "\nIn line : " << e.line << std::endl;

    }
}

void InputNode::compute()
{
    dynamic_cast<OutputClassMat*>(m_outputs[0])->setData(m_img);
}
