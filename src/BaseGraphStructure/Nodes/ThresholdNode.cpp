#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/ThresholdNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassInt.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

ThresholdNode::ThresholdNode(const std::string& name) : BaseNode(name)
{
    addInput(new InputClassMat(name + "Input", this));
    addOutput(new OutputClassMat(name + "Output", this));
}


// #define INPUT(Types, N);
void ThresholdNode::prepare()
{
    m_inImage = dynamic_cast<InputClassMat*>(m_inputs[0])->getData();
    //m_thresh = dynamic_cast<InputClassInt*>(m_inputs[1])->getData();

}

void ThresholdNode::compute()
{
    cv::threshold(m_inImage,
                  m_outImage,
                  1,
                  cv::INTER_MAX,
                  cv::THRESH_BINARY
                  );

    dynamic_cast<OutputClassMat*>(m_outputs[0])->setData(m_outImage);
}
