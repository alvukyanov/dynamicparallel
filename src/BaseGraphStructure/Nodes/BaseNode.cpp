#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/BaseNode.h>

#include <exception>
#include <iostream>

BaseNode::BaseNode()
{

}


BaseNode::BaseNode(const std::string& name)
{
    //init();
    m_name = name;
}

void BaseNode::init()
{

}

IOBaseClass* BaseNode::getInput(const std::string& name)
{
    return MGetItem(m_inputs, name);
}

IOBaseClass *BaseNode::getOutput(const std::string& name)
{
    return MGetItem(m_outputs, name);
}

void BaseNode::addInput(IOBaseClass* input)
{
    m_inputs.push_back(input);
}

void BaseNode::addOutput(IOBaseClass* output)
{
    m_outputs.push_back(output);
}

IOBaseClass* BaseNode::MGetItem(std::vector<IOBaseClass*> dataVector, const std::string& name)
{
    try
    {
        for(auto i : dataVector)
            if(i->getName() == name)
                return i;

        throw std::exception();
    }
    catch (std::exception &e) {
        std::cerr << "BaseNode::MGetItem:: got exception: " << e.what() << "\n";
    }
}

void BaseNode::run()
{
    compute();
}

std::vector<IOBaseClass*> BaseNode::MGetItems(std::vector<IOBaseClass*> dataVector)
{
    try
    {
        std::vector<IOBaseClass*> items;
        for(auto i : dataVector)
            items.push_back(i);

        return items;
    }
    catch (std::exception &e) {
        std::cerr << "BaseNode::MGetItem:: got exception: " << e.what() << "\n";
    }
}

std::vector<IOBaseClass*> BaseNode::getAllOutputs()
{
    return MGetItems(m_outputs);
}


std::vector<IOBaseClass*> BaseNode::getAllInputs()
{
    return MGetItems(m_inputs);
}

void BaseNode::setName(const std::string& name)
{
    m_name = name;
}

std::string BaseNode::getName()
{
    return m_name;
}

void BaseNode::prepare() {}


BaseNode::~BaseNode() {}

void BaseNode::compute() {}




