#include <include/vmixprocessingmodule/BaseGraphStructure/Nodes/OutputNode.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/InputClasses/InputClassMat.h>
#include <include/vmixprocessingmodule/BaseGraphStructure/OutputClasses/OutputClassMat.h>
#include <opencv2/imgcodecs.hpp>
#include <iostream>


OutputNode::OutputNode(const std::string& name, const std::string& path) : BaseNode(name)
{
    m_imagePath = path;
    addInput(new InputClassMat(name + "Input", this));
    addOutput(new OutputClassMat(name + "Output", this));
}


void OutputNode::prepare()
{
    m_img = dynamic_cast<InputClassMat*>(m_inputs[0])->getData();
}

void OutputNode::compute()
{
    cv::imwrite(m_imagePath, m_img);
    dynamic_cast<OutputClassMat*>(m_outputs[0])->setData(m_img);
}
