﻿#include <include/vmixprocessingmodule/BaseGraphStructure/Edge.h>

Edge::Edge()
{
    m_leftVertex = std::make_shared<Vertex>("leftVertex");
    m_rightVertex = std::make_shared<Vertex>("rightVertex");

    m_length = 0;

}

Edge::Edge(std::shared_ptr<Vertex> leftVertex, std::shared_ptr<Vertex> rightVertex, std::initializer_list<std::shared_ptr<Link>>& links)
{
    m_leftVertex = leftVertex;
    m_rightVertex = rightVertex;
    for(auto& i: links)
    {
        m_links.push_back(i);
    }
    m_length = 0;
}
Edge::Edge(std::shared_ptr<Vertex> leftVertex, std::shared_ptr<Vertex> rightVertex)
{
    m_leftVertex = leftVertex;
    m_rightVertex = rightVertex;
}


void Edge::addLink(std::shared_ptr<Link>& link)
{
    m_links.push_back(link);
}

void Edge::removeLink(std::shared_ptr<Link>& link)
{

}

void Edge::setLeftVertex(std::shared_ptr<Vertex> leftVertex)
{
    m_leftVertex = leftVertex;
}

void Edge::setRightVertex(std::shared_ptr<Vertex> rightVertex)
{
    m_rightVertex = rightVertex;
}


std::shared_ptr<Vertex> Edge::getLeftVertex()
{
    return m_leftVertex;
}

std::shared_ptr<Vertex> Edge::getRightVertex()
{
    return m_rightVertex;
}

void Edge::setLength(const int& len)
{
    m_length = len;
}

int Edge::getLetngth()
{
    return m_length;
}


