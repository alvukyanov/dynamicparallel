#include <include/vmixprocessingmodule/BaseGraphStructure/OutputClasses/OutputClassMat.h>

OutputClassMat::OutputClassMat(const std::string& name) : InputClassMat(name) {}

OutputClassMat::OutputClassMat(const std::string& name, BaseNode* node) : InputClassMat(name, node) {}

InputClassMat* OutputClassMat::copy(InputClassMat* input)
{
    input->setData(this->getData());
    return input;
}
